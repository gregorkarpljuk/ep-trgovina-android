package ep.trgovina;

import android.app.ActionBar;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.*;

public class CustomerEditDetails extends AppCompatActivity implements Callback<CustomerDetails> {

    private static final String TAG = CustomerEditDetails.class.getCanonicalName();

    private CustomerDetails customerDetails;

    private EditText name, surname, username, phone, street, houseNumber, town, zip;

    private String userId;
    private int userIdInteger;

    //CustomerDetails cd =  new CustomerDetails();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_edit_details);
        ActionBar actionBar = getActionBar();
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        name = (EditText) findViewById(R.id.name);
        surname = (EditText) findViewById(R.id.surname);
        username = (EditText) findViewById(R.id.username);
        phone = (EditText) findViewById(R.id.phone);
        street = (EditText) findViewById(R.id.street);
        houseNumber = (EditText) findViewById(R.id.houseNumber);
        town = (EditText) findViewById(R.id.town);
        zip = (EditText) findViewById(R.id.zip);



        if (sp.getBoolean("user_logged_in", false)) {

            try {

                userIdInteger = Integer.parseInt(sp.getString("user_id", "ERROR, NO STRING"));

                HttpApi.getInstance().getClientData(userIdInteger).enqueue(this);

            } catch (Exception ex) {
                Log.i(TAG, "ERROR, cannot cast to Integer. Is user logged in?");
            }


        } else
            Log.i(TAG, "Boolean does not appear to be set, this is it:" + sp.getBoolean("user_logged_in", false));
        //editor.putBoolean("user_logged_in", true);
        //editor.commit();

        //Log.i(TAG, "user_id: " + sp.getString("user_id", "ERROR, NO STRING"));


    }

    @Override
    public void onResponse(Call<CustomerDetails> call, Response<CustomerDetails> response) {
        customerDetails = response.body();

        Log.i(TAG, "Got result: " + customerDetails);

        userId = customerDetails.id_oseba;

        updateViews();
    }


    private void updateViews() {


        //button.setOnClickListener(this);


        name.setText(checkIfValidString(customerDetails.ime));
        surname.setText(checkIfValidString(customerDetails.priimek));
        username.setText(checkIfValidString(customerDetails.username));
        phone.setText(checkIfValidString(customerDetails.telefonska));
        street.setText(checkIfValidString(customerDetails.ulica));
        houseNumber.setText(checkIfValidString(customerDetails.stevilka));
        town.setText(checkIfValidString(customerDetails.kraj));
        zip.setText(checkIfValidString(customerDetails.posta));


    }

    private void updateUser(int userId, String surnameToSend, String nameToSend,
                            String usernameToSend, String zipToSend, String townToSend,
                            String streetToSend, String houseNumberToSend, String phoneToSend) {
        HttpApi.getInstance().changeUserData(userId, surnameToSend, nameToSend, usernameToSend,
                zipToSend, townToSend, streetToSend, houseNumberToSend, phoneToSend)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // handle success
                        // access response code with response.code()
                        // access string of the response with response.body().string()
                        Log.i(TAG, "Got response code: " + response.code());
                        Log.i(TAG, "Got result: " + response.body());

                        Toast.makeText(getActivity(), "Data successfully saved",
                                Toast.LENGTH_LONG).show();
                    /*
                    try {
                        Log.i(TAG, "Got result: " + response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    */
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // handle error
                        t.printStackTrace();
                        Log.i(TAG, "Done fucked up");
                        Toast.makeText(getActivity(), "Data wasn't successfully saved.",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void changeUserPassword(int userId, String currentPassToSend, String newPassToSend) {
        HttpApi.getInstance().changeUserPassword(userId, currentPassToSend, newPassToSend)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // handle success
                        // access response code with response.code()
                        // access string of the response with response.body().string()
                        Log.i(TAG, "Got response code: " + response.code());
                        Log.i(TAG, "Got result: " + response.body());

                        Toast.makeText(getActivity(), "Password successfully changed",
                                Toast.LENGTH_LONG).show();
                        try {
                            SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        /*
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finish();
                        */
                    /*
                    try {
                        Log.i(TAG, "Got result: " + response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    */
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // handle error
                        t.printStackTrace();
                        Log.i(TAG, "Done fucked up");
                        Toast.makeText(getActivity(), "Password wasn't successfully saved.",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void onClickSaveDataButton(View view) throws InterruptedException {
        //HttpApi.getInstance().authenticate("janez@novak.si", "qwertz").enqueue(this);

        View parentView = (View) view.getParent();

        String passwordHash = checkIfValidString(customerDetails.geslo);

        String currentPasswordField = checkIfValidString(((EditText) findViewById(R.id.currentPassword)).getText().toString());
        String newPasswordField = checkIfValidString(((EditText) findViewById(R.id.newPassword)).getText().toString());


        if (currentPasswordField.isEmpty()) {
            Toast.makeText(getActivity(), "Enter your current password to save data",
                    Toast.LENGTH_LONG).show();

        } else if (!passwordHash.equals(getSha1Hex(currentPasswordField))) {
            Log.i(TAG, "passwordHash: " + passwordHash + " getSha1Hex(currentPasswordField) " + getSha1Hex(currentPasswordField));
            Toast.makeText(getActivity(), "Wrong password!",
                    Toast.LENGTH_LONG).show();
        } else if (newPasswordField.isEmpty()) {
            Log.i(TAG, "New pass field is empty");
            saveUserData();
        } else {
            Toast.makeText(getActivity(), "Attempting to save password...",
                    Toast.LENGTH_SHORT).show();
            changeUserPassword(userIdInteger, currentPasswordField, newPasswordField);

            saveUserData();
        }
    }

    private void saveUserData() {
        Toast.makeText(getActivity(), "Attempting to save data...",
                Toast.LENGTH_SHORT).show();


        String nameToSend = name.getText().toString().trim();
        String surnameToSend = surname.getText().toString().trim();
        String usernameToSend = username.getText().toString().trim();
        String streetToSend = street.getText().toString().trim();
        String houseNumberToSend = houseNumber.getText().toString().trim();
        String townToSend = town.getText().toString().trim();
        String zipToSend = zip.getText().toString().trim();
        String phoneToSend = phone.getText().toString().trim();


        Log.i(TAG, "userIdInteger: " + userIdInteger);

        if (checkIfValidStringForToast(surnameToSend) && checkIfValidStringForToast(nameToSend)
                && checkIfValidStringForToast(usernameToSend) && checkIfValidStringForToast(zipToSend)
                && checkIfValidStringForToast(townToSend) && checkIfValidStringForToast(streetToSend)
                && checkIfValidStringForToast(houseNumberToSend) && checkIfValidStringForToast(phoneToSend) && checkPhoneNumberForToast(phoneToSend))
            updateUser(userIdInteger, surnameToSend, nameToSend, usernameToSend, zipToSend, townToSend, streetToSend, houseNumberToSend, phoneToSend);
        else
            Toast.makeText(getActivity(), "Data missing or incorrect (phone numbers must be 9 characters long)",
                    Toast.LENGTH_LONG).show();
    }

    private String checkIfValidString(String str) {
        Log.i(TAG, "checkIfValidString: " + str);
        if (str != null && !str.isEmpty()) {
            return str;
        }

        return "";
    }

    private boolean checkIfValidStringForToast(String str) {
        if (str != null && !str.isEmpty()) {
            return true;
        }

        return false;

    }

    private boolean checkPhoneNumberForToast(String str) {
        if (str.length() == 9) {
            return true;
        }

        return false;

    }


    public static String getSha1Hex(String clearString) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(clearString.getBytes("UTF-8"));
            byte[] bytes = messageDigest.digest();
            StringBuilder buffer = new StringBuilder();
            for (byte b : bytes) {
                buffer.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            return buffer.toString();
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
    }



    @Override
    public void onFailure(Call<CustomerDetails> call, Throwable t) {

    }

    public Context getActivity() {
        return this;
    }

}
