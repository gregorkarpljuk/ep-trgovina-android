package ep.trgovina;

/**
 * Created by Gregor on 12. 01. 2018.
 */

class DEPRECATEDItemInBasket {

    public String itemId;
    public String itemName;
    public String price;

    public DEPRECATEDItemInBasket(String itemId, String itemName, String price) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.price = price;
    }
}
