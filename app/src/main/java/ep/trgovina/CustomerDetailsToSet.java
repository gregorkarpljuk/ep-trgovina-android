package ep.trgovina;

import java.util.Locale;

/**
 * Created by Gregor on 10. 01. 2018.
 */

public class CustomerDetailsToSet {

    public String priimek;
    public String ime;
    public String username;
    public String posta;
    public String kraj;
    public String ulica;
    public String stevilka;
    public String telefonska;

    @Override
    public String toString() {
        return "CustomerDetailsToSet{" +
                "priimek='" + priimek + '\'' +
                ", ime='" + ime + '\'' +
                ", username='" + username + '\'' +
                ", posta='" + posta + '\'' +
                ", kraj='" + kraj + '\'' +
                ", ulica='" + ulica + '\'' +
                ", stevilka='" + stevilka + '\'' +
                ", telefonska='" + telefonska + '\'' +
                '}';
    }
}
