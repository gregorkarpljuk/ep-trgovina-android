package ep.trgovina;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDetailsActivity extends AppCompatActivity implements Callback<Item> {
    private static final String TAG = ItemDetailsActivity.class.getCanonicalName();

    private Item item;
    private TextView tvItemDetails;
    private CollapsingToolbarLayout toolbarLayout;
    private FloatingActionButton fabEdit, fabDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail_non_customer);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);

        tvItemDetails = (TextView) findViewById(R.id.tv_item_details);
        /*
        fabEdit = (FloatingActionButton) findViewById(R.id.fab_add_to_basket);
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(ItemDetailsActivity.this, CustomerSettings.class);
                intent.putExtra("ep.trgovina.item", item);
                startActivity(intent);
            }
        });
        fabDelete = (FloatingActionButton) findViewById(R.id.fab_UNUSED);
        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(ItemDetailsActivity.this);
                dialog.setTitle("Confirm deletion");
                dialog.setMessage("Are you sure?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteBook();
                    }
                });
                dialog.setNegativeButton("Cancel", null);
                dialog.create().show();
            }
        });
        */

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final int id = getIntent().getIntExtra("ep.trgovina.id_izdelka", 0);
        if (id > 0) {
            HttpApi.getInstance().get(id).enqueue(this);
        }
    }




    @Override
    public void onResponse(Call<Item> call, Response<Item> response) {
        item = response.body();
        Log.i(TAG, "Got result: " + item);

        if (response.isSuccessful()) {
            tvItemDetails.setText(item.opis_izdelka + ", " + item.postavka + " EUR");
            toolbarLayout.setTitle(item.ime_izdelka);
        } else {
            String errorMessage;
            try {
                errorMessage = "An error occurred: " + response.errorBody().string();
            } catch (IOException e) {
                errorMessage = "An error occurred: error while decoding the error message.";
            }
            Log.e(TAG, errorMessage);
            //tvItemDetails.setText(errorMessage);
        }
    }

    @Override
    public void onFailure(Call<Item> call, Throwable t) {
        Log.w(TAG, "Error: " + t.getMessage(), t);
    }
}
