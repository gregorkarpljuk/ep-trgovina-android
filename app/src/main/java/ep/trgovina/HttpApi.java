package ep.trgovina;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

import okhttp3.CookieJar;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;


public class HttpApi extends AppCompatActivity {

    private String returnCookie() {
        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        String cookie = sp.getString("cookie", "");

        return cookie;
    }



    interface HttpService {
        //todo
        //IP-ja ne spreminjaj, port pa nastavi na izbranega iz VirtualBox-a
        //todo


        String BASE_URL = "http://10.0.2.2:8080/netbeans/ep-trgovina/REST/api/";

        //  http://localhost:8080/netbeans/ep-trgovina/REST/api/

        @GET("products")
        Call<List<Item>> getAll();

        @FormUrlEncoded
        @PUT("cartProducts")
        Call<ResponseBody> addToBasket(@Header("Cookie") String cookie,
                                       @Field("id") int id_izdelka,
                                       @Field("kolicina") int kolicina);

        //localhost/netbeans/ep-trgovina/REST/api/cartProducts
        // (telo: id "idIzdelka", amount: "kolicina", --> odgovor: OK),

        @GET("cart")
        Call<List<ItemInCart>> getCart(@Header("Cookie") String cookie);

        @GET("products/{id_izdelka}")
        Call<Item> get(@Path("id_izdelka") int id_izdelka);

        @GET("client/{id_client}")
        Call<CustomerDetails> getClientData(@Path("id_client") int id_client);

        /*@FormUrlEncoded
        @PUT("client/{id_client}")
        Call<ResponseBody> changeUserData(@Path("id_client") int id_client, @Body CustomerDetailsToSet customerDetailsToSet);
        */

        @FormUrlEncoded
        @PUT("client/{id_client}")
        Call<ResponseBody> changeUserData(@Path("id_client") int id_client,
                                          @Field("priimek") String priimek,
                                          @Field("ime") String ime,
                                          @Field("username") String username,
                                          @Field("posta") String posta,
                                          @Field("kraj") String kraj,
                                          @Field("ulica") String ulica,
                                          @Field("stevilka") String stevilka,
                                          @Field("telefonska") String telefonska);

        @FormUrlEncoded
        @PUT("clientPassword/{id_client}")
        Call<ResponseBody> changeUserPassword(@Path("id_client") int id_client,
                                              @Field("passwordOld") String passwordOld,
                                              @Field("passwordNew") String passwordNew);

        //@Headers("")
        @FormUrlEncoded
        @POST("cartProducts")
        Call<ResponseBody> addToCart(@Header("Cookie") String cookie,
                                     @Field("id") int id);

        //@FormUrlEncoded
        @GET("invoices/{id_client}")
        Call<List<PastPurchase>> viewAllInvoices(@Path("id_client") int id_client);


        //(idUserja, idNakupa
        @GET("invoices/{id_client}/{id_purchase}")
        Call<List<SpecificPastPurchase>> viewSpecificPurchase(@Path("id_client") int id_client,
                                                              @Path("id_purchase") int id_purchase);

        //update košarica (PUT)  (telo: id "idIzdelka", amount: "kolicina", --> odgovor: OK)


        @FormUrlEncoded
        @PUT("cartProducts")
        Call<ResponseBody> updateBasket(@Header("Cookie") String cookie,
                                        @Field("id") int id_izdelka,
                                        @Field("amount") int kolicina);


        @DELETE("cartProducts/{id_izdelka}")
        Call<ResponseBody> deleteFromBasket(@Header("Cookie") String cookie,
                                            @Path("id_izdelka") int id_izdelka);
        //cartProducts/3

        @FormUrlEncoded
        @POST("cart")
        Call<ResponseBody> buy(@Header("Cookie") String cookie,
                               @Field("id") int id_client);


        @FormUrlEncoded
        @PUT("books/{id_izdelka}")
        Call<Void> update(@Path("id_izdelka") int id_izdelka,
                          @Field("ime_izdelka") String ime_izdelka,
                          @Field("slika_izdelka") String slika_izdelka,
                          @Field("postavka") double postavka,
                          @Field("id_statusaizdelka") int id_statusaizdelka,
                          @Field("opis_izdelka") String opis_izdelka);


        //@Headers("Content-Type: application/json")
        @FormUrlEncoded
        @POST("client")
        Call<User> authenticate(@Field("username") String username,
                                @Field("password") String password);

        //Call<Void> authenticate(@Field("username") String username,
        //                        @Field("password") String password);


        //Call<ResponseBody> login(@Path("login") String postfix, @Body RequestBody params);

        //@Headers("Content-Type: application/json")
        //@POST("login")


        /*
        @POST("client")
        Call<HttpResponse> authenticate(
                @Body User user
        );
        */

        @FormUrlEncoded
        @POST("books")
        Call<Void> insert(@Field("ime_izdelka") String ime_izdelka,
                          @Field("slika_izdelka") String slika_izdelka,
                          @Field("postavka") double postavka,
                          @Field("id_statusaizdelka") int id_statusaizdelka,
                          @Field("opis_izdelka") String opis_izdelka);




        @DELETE("books/{id_izdelka}")
        Call<Void> delete(@Path("id_izdelka") int id_izdelka);






    /*
        @GET("books")
        Call<List<Item>> getAll();

        @GET("books/{id_izdelka}")
        Call<Item> get(@Path("id_izdelka") int id_izdelka);

        @FormUrlEncoded
        @POST("books")
        Call<Void> insert(@Field("ime_izdelka") String ime_izdelka,
                          @Field("slika_izdelka") String slika_izdelka,
                          @Field("postavka") double postavka,
                          @Field("id_statusaizdelka") int id_statusaizdelka,
                          @Field("opis_izdelka") String opis_izdelka);

        @FormUrlEncoded
        @PUT("books/{id_izdelka}")
        Call<Void> update(@Path("id_izdelka") int id_izdelka,
                          @Field("ime_izdelka") String ime_izdelka,
                          @Field("slika_izdelka") String slika_izdelka,
                          @Field("postavka") double postavka,
                          @Field("id_statusaizdelka") int id_statusaizdelka,
                          @Field("opis_izdelka") String opis_izdelka);

        @DELETE("books/{id_izdelka}")
        Call<Void> delete(@Path("id_izdelka") int id_izdelka);
    */

    }


    private static HttpService instance;

    public static synchronized HttpService getInstance() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)

                .build();



        if (instance == null) {
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(HttpService.BASE_URL)

                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            instance = retrofit.create(HttpService.class);
        }

        return instance;
    }

/*
    public static void testApiRequest() {
        // Retrofit setup
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Service setup
        HttpService service = retrofit.create(HttpService.class);

        // Prepare the HTTP request
        Call<HttpResponse> call = service.authenticate(new User("janez@novak.si", "janez"));

        // Asynchronously execute HTTP request
        call.enqueue(new Callback<HttpResponse>() {
            @Override
            public void onResponse(Call<HttpResponse> call, ResponseFromAPI<HttpResponse> response) {
                // http response status code + headers
                System.out.println("ResponseFromAPI status code: " + response.code());

                // isSuccess is true if response code => 200 and <= 300
                if (!response.isSuccessful()) {
                    // print response body if unsuccessful
                    try {
                        System.out.println(response.errorBody().string());
                    } catch (IOException e) {
                        // do nothing
                    }
                    return;
                }

                // if parsing the JSON body failed, `response.body()` returns null
                HttpResponse decodedResponse = response.body();
                if (decodedResponse == null) return;
                System.out.println(decodedResponse);
                // at this point the JSON body has been successfully parsed
                System.out.println("ResponseFromAPI (contains request infos):");
                System.out.println("- url:         " + decodedResponse.url);
                System.out.println("- ip:          " + decodedResponse.origin);
                System.out.println("- headers:     " + decodedResponse.headers);
                System.out.println("- args:        " + decodedResponse.args);
                System.out.println("- form params: " + decodedResponse.form);
                System.out.println("- json params: " + decodedResponse.json);
            }

            @Override
            public void onFailure(Call<HttpResponse> call, Throwable t) {
                System.out.println("onFailure");
                System.out.println(t.getMessage());
            }


        });
    }
    */
}
