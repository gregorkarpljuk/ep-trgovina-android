package ep.trgovina;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class SpecificPastPurchaseAdapter extends ArrayAdapter<SpecificPastPurchase> {
    public SpecificPastPurchaseAdapter(Context context) {
        super(context, 0, new ArrayList<SpecificPastPurchase>());
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final SpecificPastPurchase specificPastPurchase = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_element, parent, false);
        }

        final TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        final TextView tvAuthor = (TextView) convertView.findViewById(R.id.tv_author);
        final TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);

        tvTitle.setText(specificPastPurchase.ime_izdelka);
        tvAuthor.setText(specificPastPurchase.kolicina_izdelka);
        tvPrice.setText(String.format(Locale.ENGLISH, "%.2f EUR", specificPastPurchase.postavka));

        return convertView;
    }
}
