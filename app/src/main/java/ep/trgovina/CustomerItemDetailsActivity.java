package ep.trgovina;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.util.concurrent.TimeUnit.SECONDS;

public class CustomerItemDetailsActivity extends AppCompatActivity implements Callback<Item> {
    private static final String TAG = CustomerItemDetailsActivity.class.getCanonicalName();

    private Item item;
    private TextView tvItemDetails;
    private CollapsingToolbarLayout toolbarLayout;
    private FloatingActionButton fabEdit, fabDelete;

    private String title, price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);

        tvItemDetails = (TextView) findViewById(R.id.tv_item_details);

        fabEdit = (FloatingActionButton) findViewById(R.id.fab_add_to_basket);
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addToBasket();

                /*
                SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                Gson gson = new Gson();
                String json = sp.getString("basket", null);
                Type type = new TypeToken<ArrayList<DEPRECATEDItemInBasket>>() {
                }.getType();
                //ArrayList<DEPRECATEDItemInBasket> retrievedBasket = gson.fromJson(json, type);
                ArrayList<DEPRECATEDItemInBasket> retrievedBasket = gson.fromJson(new TypeToken<ArrayList<DEPRECATEDItemInBasket>>(){}.getType());

                Log.i(TAG, "basket" + retrievedBasket);

                editor.remove("basket");
                editor.commit();

                final int id = getIntent().getIntExtra("ep.trgovina.id_izdelka", 0);

                retrievedBasket.add(new DEPRECATEDItemInBasket(String.valueOf(id), title, price));

                addItemToBasket(retrievedBasket);




                //final Intent intent = new Intent(CustomerItemDetailsActivity.this, CustomerSettings.class);
                //intent.putExtra("ep.trgovina.item", item);
                //startActivity(intent);

                */
            }
        });

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final int id = getIntent().getIntExtra("ep.trgovina.id_izdelka", 0);
        if (id > 0) {
            HttpApi.getInstance().get(id).enqueue(this);
        }
    }

    //Deprecated
    private void addItemToBasket(ArrayList updatedBasket) {
        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        Gson gson = new Gson();

        String json = gson.toJson(updatedBasket);

        editor.putString("basket", json);
        editor.commit();
    }

    private void addToBasket() {
        final int id = getIntent().getIntExtra("ep.trgovina.id_izdelka", 0);

        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        final String cookie = sp.getString("cookie", "");

        HttpApi.getInstance().addToCart(cookie, id)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // handle success
                        // access response code with response.code()
                        // access string of the response with response.body().string()
                        Log.i(TAG, "Got response code: " + response.code());
                        Log.i(TAG, "Got result: " + response.body());

                        Toast.makeText(getActivity(), "Item added to basket",
                                Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // handle error
                        t.printStackTrace();
                        Log.i(TAG, "Done f*cked up");
                        Toast.makeText(getActivity(), "Error",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }


    private void deleteBook() {
        // todo
        /*
        HttpApi.getInstance().delete(item.id_izdelka).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, ResponseFromAPI<Void> response) {
                startActivity(new Intent(ItemDetailsActivity.this, MainActivity.class));
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.w(TAG, "Napaka: "+ t.getMessage(), t);
            }
        });
        */
    }


    @Override
    public void onResponse(Call<Item> call, Response<Item> response) {
        item = response.body();
        Log.i(TAG, "Got result: " + item);

        if (response.isSuccessful()) {
            tvItemDetails.setText(item.opis_izdelka + ", " + item.postavka + " EUR");
            title = item.ime_izdelka;
            price = String.valueOf(item.postavka);
            toolbarLayout.setTitle(item.ime_izdelka);
            //toolbarLayout.setSubtitle
        } else {
            String errorMessage;
            try {
                errorMessage = "An error occurred: " + response.errorBody().string();
            } catch (IOException e) {
                errorMessage = "An error occurred: error while decoding the error message.";
            }
            Log.e(TAG, errorMessage);
            tvItemDetails.setText(errorMessage);
        }
    }

    @Override
    public void onFailure(Call<Item> call, Throwable t) {
        Log.w(TAG, "Error: " + t.getMessage(), t);
    }

    public Context getActivity() {
        return this;
    }
}
