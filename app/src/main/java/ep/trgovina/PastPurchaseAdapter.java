package ep.trgovina;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class PastPurchaseAdapter extends ArrayAdapter<PastPurchase> {
    public PastPurchaseAdapter(Context context) {
        super(context, 0, new ArrayList<PastPurchase>());
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final PastPurchase pastPurchase = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_element, parent, false);
        }

        final TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        final TextView tvAuthor = (TextView) convertView.findViewById(R.id.tv_author);
        final TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);

        tvTitle.setText("Id naročila: " + pastPurchase.id_narocilo);
        tvAuthor.setText("Id statusa: " + pastPurchase.id_statusnarocila);
        tvPrice.setText(String.format(Locale.ENGLISH, "%.2f EUR", pastPurchase.skupajPlacilo));

        return convertView;
    }
}
