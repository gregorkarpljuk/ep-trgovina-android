package ep.trgovina;

import java.io.Serializable;
import java.util.Locale;

public class ItemInCart implements Serializable {
    public int id_izdelka;
    public String ime_izdelka;
    public int id_statusaizdelka;
    public String opis_izdelka;
    public double postavka;
    public String slika_izdelka;
    public String quantity;


    /*
    public int id_izdelka, id_statusaizdelka;
    public String ime_izdelka, slika_izdelka, uri, opis_izdelka;
    public double postavka;
     */

    /*@Override
    public String toString() {
        return String.format(Locale.ENGLISH,
                "%s: %s, %d (%.2f EUR) &i",
                ime_izdelka, opis_izdelka, id_statusaizdelka, postavka, quantity);
    }
    */
}
