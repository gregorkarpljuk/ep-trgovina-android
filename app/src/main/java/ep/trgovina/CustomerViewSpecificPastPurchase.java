package ep.trgovina;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerViewSpecificPastPurchase extends AppCompatActivity implements Callback<List<SpecificPastPurchase>> {
    private static final String TAG = CustomerViewSpecificPastPurchase.class.getCanonicalName();

    private SwipeRefreshLayout container;
    private Button button;
    private ListView list;
    private SpecificPastPurchaseAdapter adapter;

    private int userIdInteger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final double skupajPlacilo = getIntent().getDoubleExtra("skupajPlacilo", 0);
        getSupportActionBar().setTitle("Skupaj: " + skupajPlacilo + "EUR");


        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        if (sp.getBoolean("user_logged_in", false)) {
            userIdInteger = Integer.parseInt(sp.getString("user_id", "ERROR, NO STRING"));
        }

        list = (ListView) findViewById(R.id.items);

        adapter = new SpecificPastPurchaseAdapter(this);
        list.setAdapter(adapter);
        /*
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final SpecificPastPurchase specificPastPurchase = adapter.getItem(i);
                if (specificPastPurchase != null) {
                    final Intent intent = new Intent(CustomerViewSpecificPastPurchase.this, CustomerInvoiceDetailsActivity.class);
                    intent.putExtra("ep.trgovina.id_izdelka", specificPastPurchase.id_narocilo);
                    startActivity(intent);
                }
            }
        });
        */

        //final String id = getIntent().getStringExtra("id_narocilo");
        //final String id = getIntent().getStringExtra("id_narocilo");

        final int id_narocilo = getIntent().getIntExtra("id_narocilo", 0);
        final int id_oseba = getIntent().getIntExtra("id_oseba", 0);
        //final double skupajPlacilo = getIntent().getDoubleExtra("skupajPlacilo", 0);

        container = (SwipeRefreshLayout) findViewById(R.id.container);
        container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HttpApi.getInstance().viewSpecificPurchase(id_oseba, id_narocilo).enqueue(CustomerViewSpecificPastPurchase.this);
            }
        });

        button = (Button) findViewById(R.id.add_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(CustomerViewSpecificPastPurchase.this, CustomerSettings.class);
                startActivity(intent);
            }
        });

        HttpApi.getInstance().viewSpecificPurchase(id_oseba, id_narocilo).enqueue(CustomerViewSpecificPastPurchase.this);
    }

    @Override
    public void onResponse(Call<List<SpecificPastPurchase>> call, Response<List<SpecificPastPurchase>> response) {
        final List<SpecificPastPurchase> hits = response.body();

        if (response.isSuccessful()) {
            Log.i(TAG, "Hits: " + hits.size());
            adapter.clear();
            adapter.addAll(hits);
        } else {
            String errorMessage;
            try {
                errorMessage = "An error occurred: " + response.errorBody().string();
            } catch (IOException e) {
                errorMessage = "An error occurred: error while decoding the error message.";
            }
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
            Log.e(TAG, errorMessage);
        }
        container.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<List<SpecificPastPurchase>> call, Throwable t) {
        Log.w(TAG, "Error: " + t.getMessage(), t);
        container.setRefreshing(false);
    }
}
