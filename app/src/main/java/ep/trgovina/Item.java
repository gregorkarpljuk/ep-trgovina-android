package ep.trgovina;

import java.io.Serializable;
import java.util.Locale;

public class Item implements Serializable {
    public int id_izdelka;
    public String ime_izdelka;
    public int id_statusaizdelka;
    public String opis_izdelka;
    public double postavka;
    public String slika_izdelka;


    /*
    public int id_izdelka, id_statusaizdelka;
    public String ime_izdelka, slika_izdelka, uri, opis_izdelka;
    public double postavka;
     */

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH,
                "%s: %s, %d (%.2f EUR)",
                ime_izdelka, opis_izdelka, id_statusaizdelka, postavka);
    }
}
