package ep.trgovina;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class CustomerInvoiceAdapter extends ArrayAdapter<Item> {
    public CustomerInvoiceAdapter(Context context) {
        super(context, 0, new ArrayList<Item>());
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final Item item = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_element, parent, false);
        }

        final TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        final TextView tvAuthor = (TextView) convertView.findViewById(R.id.tv_author);
        final TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);

        tvTitle.setText(item.ime_izdelka);
        tvAuthor.setText(item.opis_izdelka);
        tvPrice.setText(String.format(Locale.ENGLISH, "%.2f EUR", item.postavka));

        return convertView;
    }
}
