package ep.trgovina;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerViewBasket extends AppCompatActivity implements Callback<List<ItemInCart>> {
    private static final String TAG = CustomerViewBasket.class.getCanonicalName();

    private SwipeRefreshLayout container;
    private Button button;
    private ListView list;
    private ItemBasketAdapter adapter;

    public interface BtnClickListener {
        void onSubtractBtnClick(int itemID, int kolicina);

        void onAddBtnClick(int itemID, int kolicina);

        void onDeleteBtnClick(int itemID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_basket);

        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        final String userId = sp.getString("user_id", "0");

        list = (ListView) findViewById(R.id.items);

        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finishPurchase(Integer.parseInt(userId));
            }
        });

        adapter = new ItemBasketAdapter(this, new BtnClickListener() {
            @Override
            public void onSubtractBtnClick(int itemID, int kolicina) {
                Log.i(TAG, "itemID" + itemID + "kolicina" + kolicina);
                subtractOne(itemID, kolicina);

            }

            @Override
            public void onAddBtnClick(int itemID, int kolicina) {
                Log.i(TAG, "itemID" + itemID + "kolicina" + kolicina);
                addOne(itemID, kolicina);
            }

            @Override
            public void onDeleteBtnClick(int itemID) {
                Log.i(TAG, "itemID" + itemID);
                deleteItem(itemID);
            }
        });
        list.setAdapter(adapter);
        /*
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final ItemInCart item = adapter.getItem(i);
                if (item != null) {
                    final Intent intent = new Intent(CustomerViewBasket.this, CustomerItemDetailsActivity.class);
                    intent.putExtra("ep.trgovina.id_izdelka", item.id_izdelka);
                    startActivity(intent);
                }
            }
        });
        */



        final String cookie = sp.getString("cookie", "");

        container = (SwipeRefreshLayout) findViewById(R.id.container);
        container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HttpApi.getInstance().getCart(cookie).enqueue(CustomerViewBasket.this);
            }
        });


        /*
        for (int i=0; i < list.getChildCount(); i++)
        {
            list.getChildAt(i).setBackgroundColor(Color.BLUE);
        }
        */


        HttpApi.getInstance().getCart(cookie).enqueue(CustomerViewBasket.this);
    }

    private void addOne(int itemId, int kolicina) {
        updateBasket(itemId, kolicina + 1);
    }


    private void subtractOne(int itemId, int kolicina) {
        if (kolicina <= 1) {
            deleteItem(itemId);
        } else {
            updateBasket(itemId, kolicina - 1);
        }


    }

    private void deleteItem(int itemId) {
        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        final String cookie = sp.getString("cookie", "");

        HttpApi.getInstance().deleteFromBasket(cookie, itemId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // handle success
                        // access response code with response.code()
                        // access string of the response with response.body().string()
                        Log.i(TAG, "Got response code: " + response.code());
                        Log.i(TAG, "Got result: " + response.body());
                        Toast.makeText(getActivity(), "Item deleted", Toast.LENGTH_SHORT).show();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // handle error
                        t.printStackTrace();
                        Log.i(TAG, "Done fucked up");
                    }
                });
    }

    private void updateBasket(int itemId, int kolicina) {
        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        final String cookie = sp.getString("cookie", "");

        HttpApi.getInstance().updateBasket(cookie, itemId, kolicina)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // handle success
                        // access response code with response.code()
                        // access string of the response with response.body().string()
                        Log.i(TAG, "Got response code: " + response.code());
                        Log.i(TAG, "Got result: " + response.body());
                        Toast.makeText(getActivity(), "Quantity changed", Toast.LENGTH_SHORT).show();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // handle error
                        t.printStackTrace();
                        Log.i(TAG, "Done fucked up");
                    }
                });
    }

    private void finishPurchase(int userId) {

        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        final String cookie = sp.getString("cookie", "");

        HttpApi.getInstance().buy(cookie, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // handle success
                        // access response code with response.code()
                        // access string of the response with response.body().string()
                        Log.i(TAG, "Got response code: " + response.code());
                        Log.i(TAG, "Got result: " + response.body());

                        if (response.code() != 200) {
                            Toast.makeText(getActivity(), "Basket is empty",
                                    Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(getActivity(), "Purchase successful",
                                    Toast.LENGTH_LONG).show();
                    /*
                    try {
                        Log.i(TAG, "Got result: " + response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    */
                        finish();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // handle error
                        t.printStackTrace();
                        //Log.i(TAG, "Done fucked up");
                        Toast.makeText(getActivity(), "Error",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }



/*
    public void onClickMoreButton(View view) {
        LinearLayout vwParentRow = (LinearLayout)view.getParent();

        TextView child = (TextView)vwParentRow.getChildAt(0);
        Button btnChild = (Button)vwParentRow.getChildAt(1);
        btnChild.setText(child.getText());
        btnChild.setText("I've been clicked!");

        int c = Color.CYAN;

        vwParentRow.setBackgroundColor(c);
        vwParentRow.refreshDrawableState();
    }
    */




    @Override
    public void onResponse(Call<List<ItemInCart>> call, Response<List<ItemInCart>> response) {
        final List<ItemInCart> hits = response.body();


        if (response.isSuccessful()) {
            Log.i(TAG, "Hits: " + hits.size());
            adapter.clear();
            adapter.addAll(hits);
        } else {
            String errorMessage;
            try {
                errorMessage = "An error occurred: " + response.errorBody().string();
            } catch (IOException e) {
                errorMessage = "An error occurred: error while decoding the error message.";
            }
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
            Log.e(TAG, errorMessage);
        }
        container.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<List<ItemInCart>> call, Throwable t) {
        Log.w(TAG, "Error: " + t.getMessage(), t);
        container.setRefreshing(false);
    }

    public Context getActivity() {
        return this;
    }
}
