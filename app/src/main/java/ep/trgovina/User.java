package ep.trgovina;

import java.io.Serializable;
import java.util.Locale;

/**
 * Created by Gregor on 2. 01. 2018.
 */

class User implements Serializable {
    public String id;

    /*
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    */

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH,
                "%s",
                id);
    }

}
