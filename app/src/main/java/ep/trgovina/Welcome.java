package ep.trgovina;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gregor on 30. 12. 2017.
 */

public class Welcome extends AppCompatActivity implements Callback<User> {
    private static final String TAG = Welcome.class.getCanonicalName();

    private User id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

        ActionBar actionBar = getSupportActionBar();


        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);
        //SharedPreferences.Editor editor = sp.edit();
        //editor.clear();
        //editor.commit();

        if (sp.getBoolean("user_logged_in", false)) {
            final Intent intent = new Intent(Welcome.this, CustomerHome.class);
            startActivity(intent);
        }
        //editor.putBoolean("user_logged_in", true);
        //editor.commit();

        Log.i(TAG, "user_id: " + sp.getString("user_id", "ERROR, NO STRING"));


        final TextView noLogIn = (TextView) this.findViewById(R.id.noLogIn);
        final Button loginButton = (Button) this.findViewById(R.id.loginButton);


    }




    public void onClickLoginButton(View view) {
        final EditText username = (EditText) findViewById(R.id.usernameField);
        final EditText password = (EditText) findViewById(R.id.passwordField);

        /*
        User user = new User(
                username.getText().toString(),
                password.getText().toString()
        );
        */

        System.out.println("tile");
        String enteredUsername = username.getText().toString();
        String enteredPassword = password.getText().toString();

        System.out.println("tile");
        System.out.println(enteredUsername);
        System.out.println(enteredPassword);

        HttpApi.getInstance().authenticate(enteredUsername, enteredPassword).enqueue(this);


        //intent.putExtra("ep.trgovina.id_izdelka", item.id_izdelka);


        //HttpApi.getInstance().authenticate(enteredUsername, enteredPassword).enqueue(this);
    }

    public void onClickSkipLogin(View view) {
        //HttpApi.getInstance().authenticate("janez@novak.si", "qwertz").enqueue(this);
        final Intent intent = new Intent(Welcome.this, ViewAllItems.class);
        startActivity(intent);
    }


    @Override
    public void onResponse(Call<User> call, Response<User> response) {


        id = response.body();
        Log.i(TAG, "Got result: " + id);
        //Log.d(TAG, "RetroFit2.0 :RetroGetLogin: " + rawResponse.body().string());

        if (response.isSuccessful()) {
            System.out.println("response.isSuccessful()");


            String bodyString = new String(String.valueOf(response.body()));

            Log.i(TAG, "bodyString: " + bodyString);


            SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("user_id", bodyString);
            editor.putBoolean("user_logged_in", true);

            Log.i(TAG, "Set-Cookie: " + response.headers().get("Set-Cookie"));
            String cookie = response.headers().get("Set-Cookie");
            //sessionId = sessionId.replace("PHPSESSID=", "");
            cookie = cookie.replace("; path=/", "");
            System.out.println(cookie);


            editor.putString("cookie", cookie);


            //BASKET
            Gson gson = new Gson();

            ArrayList<DEPRECATEDItemInBasket> basket = new ArrayList<DEPRECATEDItemInBasket>();


            //ArrayList<DEPRECATEDItemInBasket> list = new ArrayList<DEPRECATEDItemInBasket>();

            basket.add(new DEPRECATEDItemInBasket("-1", "kekec", "15.12"));
            basket.add(new DEPRECATEDItemInBasket("-2", "bedanec", "2.3"));


            //basket.add("7");
            //basket.add("54");
            //basket.add("7");

            String json = gson.toJson(basket);

            editor.putString("basket", json);
            editor.commit();

            Log.i(TAG, "user_id: " + sp.getString("user_id", "ERROR, NO STRING"));
            Toast.makeText(getActivity(), "Welcome!", Toast.LENGTH_LONG).show();

            final Intent intent = new Intent(Welcome.this, CustomerHome.class);
            startActivity(intent);

        } else {
            Toast.makeText(getActivity(), "Make sure your email & password are correct", Toast.LENGTH_LONG).show();
            String errorMessage;
            try {
                errorMessage = "An error occurred: " + response.errorBody().string();
            } catch (IOException e) {
                errorMessage = "An error occurred: error while decoding the error message.";
            }
            Log.e(TAG, errorMessage);
        }
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {

    }

    public Context getActivity() {
        return this;
    }
}
