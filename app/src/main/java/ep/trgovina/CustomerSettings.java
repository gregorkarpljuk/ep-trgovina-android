package ep.trgovina;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerSettings extends AppCompatActivity
        implements View.OnClickListener, Callback<Void> {
    /*ZAKAJ ITEM EDIT?? GA NE RABIMO REALNO...*/
    private static final String TAG = CustomerSettings.class.getCanonicalName();

    private EditText author, title, price, description, year;

    private Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_settings);

        author = (EditText) findViewById(R.id.etAuthor);
        title = (EditText) findViewById(R.id.etTitle);
        price = (EditText) findViewById(R.id.etPrice);
        year = (EditText) findViewById(R.id.etYear);
        description = (EditText) findViewById(R.id.etDescription);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        final Intent intent = getIntent();
        item = (Item) intent.getSerializableExtra("ep.trgovina.item");
        if (item != null) {
            author.setText(item.ime_izdelka);
            title.setText(item.slika_izdelka);
            price.setText(String.valueOf(item.postavka));
            year.setText(String.valueOf(item.id_statusaizdelka));
            description.setText(item.opis_izdelka);
        }
    }

    @Override
    public void onClick(View view) {
        final String bookAuthor = author.getText().toString().trim();
        final String bookTitle = title.getText().toString().trim();
        final String bookDescription = description.getText().toString().trim();
        final double bookPrice = Double.parseDouble(price.getText().toString().trim());
        final int bookYear = Integer.parseInt(year.getText().toString().trim());


        if (item == null) { // dodajanje
            HttpApi.getInstance().insert(bookAuthor, bookTitle, bookPrice,
                    bookYear, bookDescription).enqueue(this);
        }

        else { // urejanje
            HttpApi.getInstance().update(item.id_izdelka, bookAuthor, bookTitle, bookPrice,
                    bookYear, bookDescription).enqueue(this);

        }

    }

    @Override
    public void onResponse(Call<Void> call, Response<Void> response) {
        final Headers headers = response.headers();

        if (response.isSuccessful()) {
            final int id;
            if (item == null) {
                Log.i(TAG, "Insertion completed.");
                // Preberemo Location iz zaglavja
                final String[] parts = headers.get("Location").split("/");
                id = Integer.parseInt(parts[parts.length - 1]);
            } else {
                Log.i(TAG, "Editing saved.");
                id = item.id_izdelka;
            }
            final Intent intent = new Intent(this, ItemDetailsActivity.class);
            intent.putExtra("ep.trgovina.id_izdelka", id);
            startActivity(intent);
        } else {
            String errorMessage;
            try {
                errorMessage = "An error occurred: " + response.errorBody().string();
            } catch (IOException e) {
                errorMessage = "An error occurred: error while decoding the error message.";
            }
            Log.e(TAG, errorMessage);
        }
    }

    @Override
    public void onFailure(Call<Void> call, Throwable t) {
        Log.w(TAG, "Error: " + t.getMessage(), t);
    }
}
