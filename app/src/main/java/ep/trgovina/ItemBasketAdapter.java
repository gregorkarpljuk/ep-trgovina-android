package ep.trgovina;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class ItemBasketAdapter extends ArrayAdapter<ItemInCart> {

    private static final String TAG = ItemBasketAdapter.class.getCanonicalName();

    private CustomerViewBasket.BtnClickListener mClickListener = null;

    public ItemBasketAdapter(Context context, CustomerViewBasket.BtnClickListener listener) {
        super(context, 0, new ArrayList<ItemInCart>());
        mClickListener = listener;
    }


    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final ItemInCart item = getItem(position);


        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.basket_element, parent,
                    false);

        }

        final TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        final TextView tvAmount = (TextView) convertView.findViewById(R.id.tv_author);
        final TextView tvId = (TextView) convertView.findViewById(R.id.tv_id);
        final TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);

        final Button addLessButton = (Button) convertView.findViewById(R.id.addLessButton);

        final int kolicina = Integer.parseInt(item.quantity);

        addLessButton.setTag(position);
        addLessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mClickListener != null)
                    mClickListener.onSubtractBtnClick(item.id_izdelka, kolicina);
            }
        });


        final Button addMoreButton = (Button) convertView.findViewById(R.id.addMoreButton);
        addMoreButton.setTag(position);
        addMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mClickListener != null)
                    mClickListener.onAddBtnClick(item.id_izdelka, kolicina);
            }
        });
        final Button deleteButton = (Button) convertView.findViewById(R.id.deleteButton);
        deleteButton.setTag(position);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mClickListener != null)
                    mClickListener.onDeleteBtnClick(item.id_izdelka);
            }
        });

        final int itemId = item.id_izdelka;

        tvTitle.setText(item.ime_izdelka);
        tvId.setText(Integer.toString(item.id_izdelka));
        tvAmount.setText("količina: " + item.quantity);
        tvPrice.setText(String.format(Locale.ENGLISH, "%.2f EUR", item.postavka));

        //tvTitle.getText()

        //final Button mButton = (Button) convertView.findViewById(R.id.addLessButton);

        /*
        addLessButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addLessButton.setText(Integer.toString(item.id_izdelka));



            }
        });


        addMoreButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addMoreButton.setText("More");
                //increaseQuanitiy(item.id_izdelka);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteButton.setText("Less");
            }
        });

        */

        return convertView;
    }

    /*
    private void increaseQuanitiy(int itemId) {

        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        final String cookie = sp.getString("cookie", "");

        HttpApi.getInstance().addToBasket(itemId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // handle success
                        // access response code with response.code()
                        // access string of the response with response.body().string()
                        Log.i(TAG, "Got response code: " + response.code());
                        Log.i(TAG, "Got result: " + response.body());

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // handle error
                        t.printStackTrace();
                        Log.i(TAG, "Done fucked up");
                    }
                });
    }
    */
}
