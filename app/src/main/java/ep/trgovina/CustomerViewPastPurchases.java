package ep.trgovina;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerViewPastPurchases extends AppCompatActivity implements Callback<List<PastPurchase>> {
    private static final String TAG = CustomerViewPastPurchases.class.getCanonicalName();

    private SwipeRefreshLayout container;
    private Button button;
    private ListView list;
    private PastPurchaseAdapter adapter;

    private int userIdInteger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sp = getSharedPreferences("app_data", Activity.MODE_PRIVATE);

        if (sp.getBoolean("user_logged_in", false)) {
            userIdInteger = Integer.parseInt(sp.getString("user_id", "ERROR, NO STRING"));
        }

        list = (ListView) findViewById(R.id.items);

        adapter = new PastPurchaseAdapter(this);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final PastPurchase pastPurchase = adapter.getItem(i);
                if (pastPurchase != null) {
                    //final Intent intent = new Intent(CustomerViewPastPurchases.this, CustomerInvoiceDetailsActivity.class);

                    final Intent intent = new Intent(CustomerViewPastPurchases.this, CustomerViewSpecificPastPurchase.class);
                    intent.putExtra("id_narocilo", pastPurchase.id_narocilo);
                    intent.putExtra("id_oseba", pastPurchase.id_oseba);
                    intent.putExtra("skupajPlacilo", pastPurchase.skupajPlacilo);
                    startActivity(intent);
                }
            }
        });

        container = (SwipeRefreshLayout) findViewById(R.id.container);
        container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HttpApi.getInstance().viewAllInvoices(userIdInteger).enqueue(CustomerViewPastPurchases.this);
            }
        });

        button = (Button) findViewById(R.id.add_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(CustomerViewPastPurchases.this, CustomerSettings.class);
                startActivity(intent);
            }
        });

        HttpApi.getInstance().viewAllInvoices(userIdInteger).enqueue(CustomerViewPastPurchases.this);
    }

    @Override
    public void onResponse(Call<List<PastPurchase>> call, Response<List<PastPurchase>> response) {
        final List<PastPurchase> hits = response.body();

        if (response.isSuccessful()) {
            Log.i(TAG, "Hits: " + hits.size());
            adapter.clear();
            adapter.addAll(hits);
        } else {
            String errorMessage;
            try {
                errorMessage = "An error occurred: " + response.errorBody().string();
            } catch (IOException e) {
                errorMessage = "An error occurred: error while decoding the error message.";
            }
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
            Log.e(TAG, errorMessage);
        }
        container.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<List<PastPurchase>> call, Throwable t) {
        Log.w(TAG, "Error: " + t.getMessage(), t);
        container.setRefreshing(false);
    }
}
